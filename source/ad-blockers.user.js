// ==UserScript==
// @name        Block ads in websites.
// @description Block ads in websites.
// @namespace   ad-blockers
// @version     1.5
// @author      deliberate.minimalist@gmail.com
// @run-at      document-start
// @downloadURL https://gitlab.com/rbprogrammer/user-scripts/-/raw/main/source/ad-blockers.user.js
// @updateURL   https://gitlab.com/rbprogrammer/user-scripts/-/raw/main/source/ad-blockers.user.js
// @match       https://www.google.com/search*
// @match       https://www.reddit.com/*
// @grant       GM_addStyle
// ==/UserScript==

GM_addStyle (`
  /* Google */
  #tads,                          /* ??? */
  .ads-ad,                        /* paid search results */
  .commercial-unit-desktop-rhs    /* right-hand panel of ads */
  {
    display: none !important;
  }

  /* Reddit */
  .promotedlink {
    display: none !important;
  }
`);
